# pyshare-logs

Repository of grafana loki configuration, which collects pyshare logs of backend & sandbox and send it to grafana cloud.

## Usage

You need to place secret in file at `secrets/grafana_cloud_password`.

Corresponding configuration is in `promtail-local-config.yaml`, `clients / basic_auth`

